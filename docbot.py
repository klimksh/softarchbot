#!/usr/bin/python
# -*- coding: utf-8  -*-
# (C) SEBIS TUM, 2015

import sys
import json
import difflib
import filecmp
import argparse
import datetime
import os
import pywikibot
from phabricator import Phabricator
from wmfphablib import Phab as phabmacros  # Cleaner (?), see wmfphablib/phabapi.py
from wmfphablib import config
from git import Git


class Docbot:
    def __init__(self):
        parser = argparse.ArgumentParser()
        parser.add_argument("-p", "--page", type=lambda s: unicode(s, sys.stdin.encoding),
                            required=True, help="component to be read and compared")
        parser.add_argument("-pp", "--phab_project", type=lambda s: unicode(s, sys.stdin.encoding),
                            required=True, help="name of Phabricator project for imported tasks")
        self.args = parser.parse_args()
        self.set_page()
        with open('settings.json') as data_file:
            self.data = json.load(data_file)

    def set_page(self):
        self.site = pywikibot.Site()
        page_title = self.args.page
        self.page = pywikibot.Page(pywikibot.Link(page_title, self.site))

    def set_task(self, title):
        global p_id
        phab = Phabricator(config.phab_user,
                           config.phab_cert,
                           config.phab_host)
        phab.update_interfaces()
        phabm = phabmacros('', '', '')
        phabm.con = phab
        phab_project_name = self.args.phab_project
        # DEBUG to verify the API connection worked:
        pywikibot.output(u"API connection details : %s " % phab.user.whoami())
        response = phab.project.query(names=[phab_project_name])
        for proj_info in response.data.values():
            if proj_info["name"] == phab_project_name:
                pywikibot.output(u"Phabricator project %s has PHID %s" % (phab_project_name, proj_info["phid"]))
                p_id = proj_info["phid"]
        taskinfo = {
            'title': 'update document : ' + title,
            'description': 'documentation mismatch in mediawiki.org page and source code',
            'ownerPHID': None,
            'ccPHIDs': [],
            'projectPHIDs': [p_id],
            'auxiliary': None
        }
        ticket = phab.maniphest.createtask(
            title=taskinfo['title'],
            description=taskinfo['description'],
            projectPHIDs=taskinfo['projectPHIDs'],
            ownerPHID=taskinfo['ownerPHID'],
            ccPHIDs=taskinfo['ccPHIDs'],
            auxiliary=taskinfo['auxiliary']
        )
        pywikibot.output(u"Created task: T%s (%s) " % (ticket['id'], ticket['phid']))

    def run(self):

        global text, fp1, fn1, fp2, fn2, f, fn, days_diff, d1, d2
        g = Git(self.data["mediawiki"])
        pull_resp = g.pull()
        pywikibot.output(u"git response %s" % pull_resp)
        if 'error' in pull_resp:
            pywikibot.output(u"could not pull changes")
            return
        else:
            self.site.login()
            try:
                text = self.page.get()
                title = self.page.title()
            except pywikibot.NoPage:
                pywikibot.output(u"Page %s does not exist; skipping." % self.page.title(asLink=True))
            except pywikibot.IsRedirectPage:
                pywikibot.output(u"Page %s is a redirect; skipping." % self.page.title(asLink=True))
            else:
                fn1 = os.path.join(self.data["readPages"], title)
                fp1 = open(fn1, 'r+')
                fp1.write(text)
                fn2 = os.path.join(self.data["mediawiki"], 'docs', title.lower())
                fp2 = open(fn2, 'r')
                result = filecmp.cmp(fn1, fn2, shallow=False)
                if result:
                    pywikibot.output(u"no change")
                else:
                    pywikibot.output(u"text diff")
                    pywikibot.output(u"mediawiki page \" %s \" last modified by \" %s \" at timestamp \" %s \"" % (
                        title, self.page.latest_revision.user, self.page.latest_revision.timestamp))
                    d1 = datetime.datetime.date(datetime.datetime.now())
                    d2 = datetime.datetime.date(self.page.latest_revision.timestamp)
                    days_diff = d1 - d2
                    if (days_diff.days > 5):
                        # create a list of lines in text1
                        text1_contents = text.splitlines(True)
                        with open(fn2, "r") as file2:
                            file2_contents = file2.readlines()
                        diff_instance = difflib.Differ()
                        diff_list = list(diff_instance.compare(text1_contents, file2_contents))
                        pywikibot.output(u"Lines different in mediawiki.org and source files")
                        fn = os.path.join(self.data["logs"], title)
                        f = open(fn, 'w')
                        for line in diff_list:
                            if line[0] == '-':
                                f.write(line),
                        f.close()
                        self.set_task(title)
            fp1.close()
            fp2.close()


def main():
    app = Docbot()
    app.run()


if __name__ == "__main__":
    main()
