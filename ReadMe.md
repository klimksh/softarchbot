# Deployment process #
1. Clone mediawiki source code repository from github (https://github.com/wikimedia/mediawiki). This repository will be used for periodically pulling the latest changes from master branch (for tracking changes in /docs folder)

2. Make sure that the pywikibot is correctly installed (set up the wiki family - https://www.mediawiki.org/wiki/Manual:Pywikibot/Use_on_third-party_wikis) 
Also, the config file (config.py) should be available in the same folder as the docbot.py script

3. Get a bot user account on the mediawiki installation and add a secretfile for the bot user-password combination.

4. Get a bot account on mediawiki phabricator.

5. Set a config file for using Phabricator APIs - phabtools.conf - in the same folder as docbot.py. See the example: phabtools.conf.example
    
# To start #
1. activate the environment (virtualenv for python - ./env/Scripts/activate)
2. Run the python script : python docbot,py -p "text_file_name" -pp "prabricator_project_name" (eg) python docbot.py -p Database.txt -pp "Software Architecture Documentation"